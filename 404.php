<?php
/**
 * Template name: 404
 *
 * @package TeatroPrevent
 */	

    $path = get_template_directory_uri();

    get_header();
?>
 <main class="404 blog">
    <div class="container">
    
      <div class="blog__content">
		<h2 class="blog__title-page title__section" style="font-size: 4.9vw;">404 - Página não encontrada</h2>
		<div style="text-align: center;">
			<p>Parece que essa página não existe.</p>
			<p>Fique por dentro dos últimos posts do nosso blog.</p>
		</div>
        <?php  
					$args = array(
						'post_type' => 'post',
						'order'			=> 'DESC',
						'posts_per_page' => 1,
						'tax_query' => array(
				        array(
			            'taxonomy' => 'category',
			            'field'    => 'slug',
			            'terms'    => 'destaque',
				        ),
					    ),
					);
					
					$loop = new WP_Query( $args );
					if( $loop->have_posts() ):
						while( $loop->have_posts() ):
							$loop->the_post();
							$category = end(get_the_category(get_the_ID()));
				?>
	        <div class="featured">
	          <a href="<?=get_permalink(get_the_ID())?>" class="featured__image">
	            <img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="Teatro Prevent Senior"/>
	          </a>
	          <div class="featured__content red white-text">
	            <div class="featured__infos">
	              <span class="featured__infos--date"><?=the_date()?></span>
	              <span class="featured__infos--label"><?=$category->name?></span>
	            </div>
	            <a href="<?=get_permalink(get_the_ID())?>" class="featured__title white-text"><?=the_title()?></a>
	            <div class="featured__exerpt"><?=the_excerpt()?></div>
	          </div>
	        </div>
        <?php 
	        	endwhile;
	        endif; 
	        wp_reset_query(); 
	      ?>
      </div>
      <ul class="post">
	      <?php  
					$args = array(
						'post_type' => 'post',
						'order'			=> 'DESC',
						'category__not_in' => 5,
						'posts_per_page' => 3
					);
					
					$loop = new WP_Query( $args );
					if( $loop->have_posts() ):
						while( $loop->have_posts() ):
							$loop->the_post();
				?>
        <li class="post__item">
          <div class="post__item--image">
            <img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="<?=the_title()?>"/>
          </div>
          <div class="post__item--content">
            <div class="post__item--title"><?=the_title()?></div>
            <p class="post__item--text"><?=the_excerpt()?></p>
            <div class="post__item--date"><?=the_date()?></div>
            <a href="<?=get_permalink(get_the_ID())?>" class="post__item--button button">Leia mais</a>
          </div>
        </li>
        <?php 
	        	endwhile;
	        endif; 
	        wp_reset_query(); 
	      ?>
      </ul>
    </div>
  </main>
<?php get_footer(); ?>