<?php
	get_header();
	
	$path = get_template_directory_uri();
	$category = end(get_the_category(get_the_ID()));
	
	while( have_posts() ):
		the_post();
		$thisID = get_the_ID();
?>

	<main class="single">
    <section class="single__banner flex-row">
      <?php echo wp_get_attachment_image($cover_image, 'full') ?>
    </section>
    <section class="single__content grey">
      <div class="container indentation__mid-page">
        <div class="single__content--head indentation__mid-page">
          <span class="single__content--category button"><?=$category->name?></span>
          <span class="single__content--date"><?php the_date(); ?></span>
<!--           <span class="single__content--tag">Prêmios</span> -->
        </div>
        <h1 class="single__title indentation__mid-page"><?=the_title()?></h1>
        
        <article class="single__article indentation__mid-page">
          <?=the_content()?>
        </article>

        <div class="single__share">
          <span class="single__share--label">COMPARTILHE O POST</span>
          <a href="https://www.facebook.com/sharer/sharer.php?<?=get_permalink(get_the_ID())?>" target="_blank" class="single__share--link">
            <img src="<?=$path?>/assets/images/facebook-black.svg" alt="Facebook - Teatro Prevent Senior"/>
          </a>
          <a href="http://www.linkedin.com/shareArticle?url=<?=get_permalink(get_the_ID())?>&amp;title=<?=the_title()?>" target="_blank" class="single__share--link">
            <img src="<?=$path?>/assets/images/linkedin-black.svg" alt="LinkedIn - Teatro Prevent Senior"/>
          </a>
          <a href="https://twitter.com/intent/tweet?text=<?=get_permalink(get_the_ID())?>" target="_blank" class="single__share--link">
            <img src="<?=$path?>/assets/images/twitter-black.svg" alt="Twitter - Teatro Prevent Senior"/>
          </a>
        </div>
      </div>
    </section>
	<?php endwhile; wp_reset_query(); ?>
    <section class="single__blog">
      <div class="container">
        <ul class="blog__list post">
	        <?php  
						$args = array(
							'post_type' => 'post',
							'order'			=> 'DESC',
							'posts_per_page' => 3,
							'post__not_in' => array($thisID)
						);
						
						$loop = new WP_Query( $args );
						if( $loop->have_posts() ):
							while( $loop->have_posts() ):
								$loop->the_post();
								$category = end(get_the_category(get_the_ID()));
					?>
	          <li class="post__item">
		          <a href="<?=get_permalink(get_the_ID())?>" class="post__item--image">
		            <img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="<?=the_title()?>"/>
		          </a>
		          <a href="<?=get_permalink(get_the_ID())?>" class="post__item--content">
		            <div class="post__item--title"><?=the_title()?></div>
		            <p class="post__item--text"><?=the_excerpt()?></p>
		            <div class="post__item--date"><?=the_date()?></div>
		            <span class="post__item--button button">Leia mais</span>
		          </a>
		        </li>
          <?php 
		        	endwhile;
		        endif; 
		        wp_reset_query(); 
		      ?>
        </ul>
        <a href="<?=bloginfo('url')?>/blog" class="button border black-text margin-auto">Acessar o blog</a>
      </div>
    </section>
  </main>

<?php get_footer(); ?>