var swiperBanner = new Swiper('.banner__container', {
  slidesPerView: 1,
  spaceBetween: 0,
  allowTouchMove: false,
  navigation: {
    nextEl: '.banner__navigation--button.next',
    prevEl: '.banner__navigation--button.prev',
  },
  pagination: {
    el: '.banner__container .swiper-pagination',
    type: 'bullets',
    clickable: true
  },
  breakpoints: {
    768: {
      allowTouchMove: true,
    }
  }
});

var swiperStructure = new Swiper('.structure__slider', {
  slidesPerView: 1,
  spaceBetween: 0,
  allowTouchMove: false,
  navigation: {
    nextEl: '.structure__navigation--button.next',
    prevEl: '.structure__navigation--button.prev',
  },
  breakpoints: {
    768: {
      allowTouchMove: true,
    }
  },
});

var swiperGallery = new Swiper('.swiper-gallery', {
  slidesPerView: 1,
  spaceBetween: 0,
  navigation: {
    nextEl: '.swiper-gallery__navigation--button.next',
    prevEl: '.swiper-gallery__navigation--button.prev',
  },
  pagination: {
    el: '.swiper-gallery__pagination',
    clickable: true
  },
});

var swiperPrograming = new Swiper('.swiper-programming', {
  slidesPerView: 'auto',
  spaceBetween: 30,
  breakpoints: {
    768: {
      spaceBetween: 15,
      // centeredSlides: true
    }
  },
  pagination: {
    el: '.swiper-programming .slider__pagination',
    clickable: true
  },
  navigation: {
    nextEl: '.programming__navigation--button.next',
    prevEl: '.programming__navigation--button.prev',
  },
});

var swiperCustumers = new Swiper('.customers__slide', {
  slidesPerView: 'auto',
  spaceBetween: 30,
  navigation: {
    nextEl: '.customers__navigation--button.next',
    prevEl: '.customers__navigation--button.prev',
  },
  pagination: {
    el: '.customers__pagination',
    clickable: true
  },
  breakpoints: {
    768: {
      spaceBetween: 15,
    }
  }
});

$('.pop-up').magnificPopup({type:'image'});

$('.pop-up').on('click', function(e){
  e.preventDefault();
  return false;
});

$(document).on('click', '.hamburguer', function() {
  $(this).toggleClass('active');
  $('.menu').toggleClass('active');
});

$(document).on('click', '.gallery__tab--link', function() {
  var content = $(this).data('tab');

  $('.gallery__tab--link').removeClass('active');
  $(this).addClass('active');

  $('.gallery__content').fadeOut(300);
  setTimeout(() => {
    $('#'+content).fadeIn(300);
  }, 300);
});

$('.select-gallery').change(function(){
	var content = $(this).val();
  
  $('.gallery__tab--link').removeClass('active');
  $(this).addClass('active');

  $('.gallery__content').fadeOut(300);
  setTimeout(() => {
    $('#'+content).fadeIn(300);
  }, 300);
})

$('.open-popup-link').magnificPopup({
  type:'inline',
});

$('.select').change(function(){
	var optionSelected = $(this).val();
	var thisLocation = window.location.href;
	
	var typeSelect = $(this).attr('id');
	
	if (typeSelect != 'tipoSelect') {
		console.log(typeSelect)
		
		$('#tipoSelect').html('');
		
	} else {
		
		$('#quandoSelect').html('');
		
	}
  
  $('.calendar__header--filters').submit();
})


$('.image-link').magnificPopup({
  type:'image',
});

var seasonsTitle = document.querySelectorAll('.seasons__filter--title');
  
var seasonsMonth = document.querySelectorAll('.seasons__filter--all--header-month_title');
var seasonsContent = document.querySelectorAll('.seasons__filter--all--content--list-item');

seasonsTitle.forEach(function(title) {

  title.addEventListener('click', function() {
    var month = event.target;
    month.nextElementSibling.classList.toggle('show-season');
    month.classList.toggle('show-season');
      
    seasonsMonth.forEach(function(item) {
  
      seasonsContent.forEach(function(subitem) {
        if (item.dataset.event === subitem.dataset.event) {
          item.style.pointerEvents = 'all';
          item.style.color = '#000';
        }
      })
    })
  }, false);

})

$('.seasons__filter--all--header-month li').click(function() {
  $(this).parent().find('li').removeClass('active');
  $(this).addClass('active');
  $(this).parent().parent().next().addClass('active');

  var targetMonth = $(this).attr('data-event').split('-')[0];
  var targerEventMonth = '';
  $($(this).parent().parent().next().children().children()).each(function(index, element) {
    targerEventMonth = $(this).attr('data-event').split('-')[0];

    if(targetMonth == targerEventMonth){
      $(this).css('display', 'block');
    }
    else{
      $(this).css('display', 'none');
    }
    
  });
  
});

//Scroll menu ancora página de Ingressos
$('.ingressos .ingressos__menu .ingressos__menu--item').click(function(){
  var menu_ancora = $(this).children()[0].hash;

  if (menu_ancora.length) {
      $('html, body').animate({ 
        scrollTop: $(menu_ancora).offset().top-90
      }, 900);
      return false;
  }
});
