//Menu desktop página de Ingressos
var footer = document.querySelector('footer');
var menu_ingressos = document.querySelector('.ingressos__menu');

window.addEventListener('scroll', function() {
  if (footer.getBoundingClientRect().y <= this.innerHeight) {
    menu_ingressos.style.display = 'none';
  } else {
    menu_ingressos.style.display = 'block';
  }
}, false);

$(document).ready(function () {
    $("body").scroll(function() {
      if($(this).scrollTop() == 0){
        $(".button_top").css("display", "none");
      } else {
        $(".button_top").css("display", "block");
      }
    });
    $(".button_top").click(function() {
      $("html, body").animate({scrollTop: 0}, 800);
    });
});