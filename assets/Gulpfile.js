var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify-es').default;

gulp.task('styles', function() {
  return gulp.src('./sass/**/*.scss')
	  .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./css'));
});

//Watch task
gulp.task('default',function() {
  gulp.watch('./sass/**/*.scss', gulp.series('styles'));
});