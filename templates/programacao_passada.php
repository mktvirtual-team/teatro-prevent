<?php
/**
 * Template name: Programação Passada
 *
 * @package TeatroPrevent
 */	
	$path = get_template_directory_uri();
	
	$tipo = $_GET['tipo'];
	$quando = $_GET['quando'];
	
	$selectTipo = get_categories(
		array(
            'post_type' => 'evento',
            'orderby' => 'id',
            'order' => 'ASC',
            'taxonomy' => 'tipo',
            
	    )
	);
	$selectQuando = get_categories(
		array(
		  'orderby' => 'name',
		  'order' => 'ASC',
		  'taxonomy' => 'data'
	  )
    );
    
    $seasons = get_terms( array(
        'taxonomy' => 'season_event',
        // 'meta_query' => array(
        //     array(
        //         'key' => 'data',
        //         'value' => date("Y-m-d"),
        //         'compare' => '<',
        //         'type' => 'DATE'
        //     )
        // )
    ));

	get_header();
?>

	<main class="programacao grey">

        <section class="calendar">
            <div class="container">
                <div class="calendar__header indentation__page">
                <h2 class="title__section">Espetáculos<br/> Realizados</h2>
                <!-- <form class="calendar__header--filters">
                    <label>Filtrar por: </label>
                    <select id="tipoSelect" class="select" name="tipo">
                        <option disabled selected>Tipo de Evento</option>
                        <?php foreach($selectTipo as $option): ?>
                            <option <?=($option->slug == $tipo) ? 'selected' : ''?> value="<?=$option->slug?>"><?=$option->name?></option>
                        <?php 
                            endforeach; 
                            
                            if($tipo || $quando): ?>
                                    <option value="todos">Ver todos</option>
                            <?php endif; ?>
                    </select>
                    <?php /* ?>
                    <select id="quandoSelect" class="select" name="quando">
                    <option disabled selected>Quando</option>
                        <?php  
                                            $args = array(
                                                'post_type' => 'evento',
                                                'order'			=> 'DESC',
                                                'orderby'   => 'meta_value_num',
                                                'meta_key'  => 'data',
                                                'meta_query' => array(
                                                    array(
                                                        'key' => 'data',
                                                        'value' => date("Y-m-d"),
                                                        'compare' => '<',
                                                        'type' => 'DATE'
                                                    )
                                                )
                                            );
                                            
                                            $loop = new WP_Query( $args );
                                            if( $loop->have_posts() ):
                                                $auxH = '';
                                                while( $loop->have_posts() ):
                                                    $loop->the_post();
                                                    $category = get_the_terms(get_the_ID(), 'tipo');
                                                    
                                                    $auxH = ($auxH != get_field('data')) ? get_field('data') : null;
                                                    
                                                    if($auxH != null):
                                                    
                                                        $time = strtotime($auxH);
                                                        
                                                        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                                                        date_default_timezone_set('America/Sao_Paulo');
                                                        
                                                        $newDate = strftime('%d de %B', strtotime($auxH));
                                        ?>
                            <option <?=($auxH == $quando) ? 'selected' : ''?> value="<?=$auxH?>"><?=utf8_encode($newDate)?></option>
                        <?php 
                                    endif;
                                        endwhile;
                                    endif; 
                                    wp_reset_query();
                                        ?>
                    </select> <?php */ ?>
                </form> -->
                </div>
                <div class="seasons__filter indentation__page">
                    <?php foreach($seasons as $season): ?>

                        <h3 class="seasons__filter--title">Temporada <?php echo $season->name ?></h3>

                        <div class="seasons__filter--all">

                            <div class="seasons__filter--all--header">
                                <ul class="seasons__filter--all--header-month">
                                    <li class="seasons__filter--all--header-month_title" data-event="01-<?php echo $season->name ?>">Jan</li>
                                    <li class="seasons__filter--all--header-month_title" data-event="02-<?php echo $season->name ?>">Fev</li>
                                    <li class="seasons__filter--all--header-month_title" data-event="03-<?php echo $season->name ?>">Mar</li>
                                    <li class="seasons__filter--all--header-month_title" data-event="04-<?php echo $season->name ?>">Abr</li>
                                    <li class="seasons__filter--all--header-month_title" data-event="05-<?php echo $season->name ?>">Mai</li>
                                    <li class="seasons__filter--all--header-month_title" data-event="06-<?php echo $season->name ?>">Jun</li>
                                    <li class="seasons__filter--all--header-month_title" data-event="07-<?php echo $season->name ?>">Jul</li>
                                    <li class="seasons__filter--all--header-month_title" data-event="08-<?php echo $season->name ?>">Ago</li>
                                    <li class="seasons__filter--all--header-month_title" data-event="09-<?php echo $season->name ?>">Set</li>
                                    <li class="seasons__filter--all--header-month_title" data-event="10-<?php echo $season->name ?>">Out</li>
                                    <li class="seasons__filter--all--header-month_title" data-event="11-<?php echo $season->name ?>">Nov</li>
                                    <li class="seasons__filter--all--header-month_title" data-event="12-<?php echo $season->name ?>">Dez</li>
                                </ul>    
                            </div>

                            <?php $events = get_events_by_season($season); ?>

                            <div class="seasons__filter--all--content">
                                <ul class="seasons__filter--all--content--list">
                                
                                    <?php while($events->have_posts()): $events->the_post(); ?>

                                        <?php 
                                            $date_event = new DateTime(get_field('data'));
                                            $month_event = $date_event->format("m");
                                            $year_event = $date_event->format("Y");
                                        ?>
                                        <li class="seasons__filter--all--content--list-item" data-event="<?php echo $month_event; ?>-<?php echo $year_event ?>">
                                            <img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="Calendário Teatro Prevent Senior"/>
                                            <div class="seasons__filter--all--content--list-item_label"><?php echo get_event_category_name(); ?></div>
                                            <h5 class="seasons__filter--all--content--list-item_title"><?php echo get_the_title(); ?></h5>
                                        </li>
                                    <?php endwhile; wp_reset_postdata(); ?>

                                </ul>
                            </div>

                        </div>

                    <?php endforeach; ?>
                </div>
                <?php /*if($quando || $tipo != 'todos' && $tipo != ''): ?>
                        <div class="calendar__content">
                            <div class="calendar__content--date-wrapper">
                                <ul class="calendar__list">
                                <?php  
                                    $taxQuery = [];
                                    $metaQuery = [];
                                            
                                            if ($quando) {
                                                $metaQuery = array(
                                                array(
                                                    'key'     => 'data',
                                                    'value'   => $quando,
                                                )
                                                );
                                            } elseif ($tipo) {
                                                $taxQuery = array(
                                                array(
                                                    'taxonomy' => 'tipo',
                                                    'field'    => 'slug',
                                                    'terms'    => $tipo
                                                )
                                                );
                                            }
                                    
                                                $args = array(
                                                    'post_type'  => 'evento',
                                                    'order'			 => 'DESC',
                                                    'tax_query'  => $taxQuery,
                                                    'meta_query' => $metaQuery,
                                                    'meta_query' => array(
                                                        array(
                                                            'key' => 'data',
                                                            'value' => date("Y-m-d"),
                                                            'compare' => '<',
                                                            'type' => 'DATE'
                                                        )
                                                    )
                                                );
                                                
                                                $loop = new WP_Query( $args );
                                                if( $loop->have_posts() ):
                                                    while( $loop->have_posts() ):
                                                        $loop->the_post();
                                            ?>
                                <li class="calendar__item indentation__full-page">
                                    <div class="calendar__item--image">
                                    <img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="Calendário Teatro Prevent Senior"/>
                                    </div>
                                    <div class="calendar__item--content">
                                    <div class="calendar__item--label"><?= get_event_category_name(); ?></div>
                                    <h5 class="calendar__item--title"><?=the_title()?></h5>
                                    <div class="calendar__item--text"><?=the_excerpt()?></div>
                                    <div class="calendar__item--date"><?=get_field('sub-titulo', get_the_ID())?></div>
                                    <a href="<?=get_permalink(get_the_ID())?>" class="button">Saiba Mais</a>
                                    </div>
                                </li>
                            <?php 
                                            endwhile;
                                            
                                            else:
                                    ?>
                                    <p style="text-align: center">Nenhum resultado para esse filtro.</p>
                                    <?php
                                        endif; 
                                        wp_reset_query();
                                            ?>
                            </ul>
                            </div>
                        </div>
                    <?php else: ?>
                        <!-- <div class="calendar__content">
                            <div class="calendar__content--date-wrapper">
                                <ul class="calendar__list">
                                    <?php  
                                                    $args = array(
                                                        'post_type' => 'evento',
                                                        'order'			=> 'DESC',
                                                        'orderby'   => 'meta_value_num',
                                                        'meta_key'  => 'data',
                                                        'meta_query' => array(
                                                            array(
                                                                'key' => 'data',
                                                                'value' => date("Y-m-d"),
                                                                'compare' => '<',
                                                                'type' => 'DATE'
                                                            )
                                                        )
                                                    );
                                                    
                                                    $loop = new WP_Query( $args );
                                                    if( $loop->have_posts() ):
                                                        $auxH = '';
                                                        while( $loop->have_posts() ):
                                                            $loop->the_post();
                                                            
                                                            $auxH = ($auxH != get_field('data')) ? get_field('data') : null;
                                                            
                                                            if ($auxH != null) {
                                                                $time = strtotime($auxH);
                                                                
                                                                setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                                                                date_default_timezone_set('America/Sao_Paulo');
                                                                
                                                                $newDate = strftime('%d de %B de %Y', strtotime($auxH)); 
                                                                $newDate = utf8_encode($newDate); 
                                                            }
                                                ?>
                                                    <?php if($auxH != null): ?>
                                                        <div class="calendar__content--date-title indentation__full-page"><?=$newDate?></div>
                                                    <?php endif; ?>
                                    <li class="calendar__item indentation__full-page">
                                        <div class="calendar__item--image">
                                        <img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="Calendário Teatro Prevent Senior"/>
                                        </div>
                                        <div class="calendar__item--content">
                                        <div class="calendar__item--label"><?= get_event_category_name() ?></div>
                                        <h5 class="calendar__item--title"><?=the_title()?></h5>
                                        <div class="calendar__item--text"><?=the_excerpt()?></div>
                                        <div class="calendar__item--date"><?=get_field('sub-titulo', get_the_ID())?></div>
                                        <a href="<?=get_permalink(get_the_ID())?>" class="button">Saiba Mais</a>
                                        </div>
                                    </li>
                                <?php 
                                                endwhile;
                                            endif; 
                                            wp_reset_query();
                                                ?>
                                </ul>
                            </div>
                        </div> -->
                <?php endif; */?>
            </div>
        </section>
    </main>

<?php get_footer(); ?>