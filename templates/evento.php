<?php
/**
 * Template name: Faça seu evento
 *
 * @package TeatroPrevent
 */	
	get_header();
	
	$path = get_template_directory_uri();
?>

	<main class="faca-seu-evento">
    <section class="banner">
      <img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="<?=the_title()?>"/>
    </section>
  
    <section class="content grey">
      <div class="container indentation__mid-page">
        <div class="content__item initial grey">
          <h1 class="content__item--title title__section"><?=get_field('titulo_pagina')?></h1>
          <div class="content__item--text indentation__mid-page">
            <p><?=get_field('texto_pagina')?></p>
          </div>
        </div>
        <div class="content__item grey">
          <h1 class="content__item--title title__section">Estrutura</h1>
          <div class="content__item--text indentation__mid-page">
            <p><?=get_field('texto_estrutura')?></p>
          </div>
        </div>
      </div>
    </section>

    <section class="structure grey">
      <div class="container">
	      <?php  
					$args = array(
						'post_type' => 'estrutura',
						'order'			=> 'ASC',
						'orderby' => 'meta_value_num',
						'meta_key'  => 'ordem'
					);
					
					$loop = new WP_Query( $args );
					if( $loop->have_posts() ):
						$cout = 1;
						while( $loop->have_posts() ):
							$loop->the_post();
							
							$metas = get_post_meta( get_the_ID() );
							$gallery = $metas['galeria'];				
							$typeBlock = ($cout % 2 == 0) ? 'block-left' : 'block-right';				
				?>
        <div class="structure__<?=$typeBlock?>">
	        <?php if($gallery): ?>
          <div class="structure__gallery">
            <div class="swiper-container swiper-gallery">
              <div class="swiper-gallery__navigation">
                <div class="swiper-gallery__navigation--button prev arrow__left"></div>
                <div class="swiper-gallery__navigation--button next arrow__right"></div>
              </div>
              <div class="swiper-wrapper">
	              <?php foreach($gallery as $image): ?>
	                <div class="swiper-slide">
	                  <div class="swiper-gallery__image image-frame">
	                    <img src="<?=pods_image_url($image, 'full')?>" alt="Teatro Prevent Senior"/>
	                  </div>
	                </div>
                <?php endforeach; ?>
              </div>
              <div class="swiper-gallery__pagination swiper-pagination"></div>
            </div>
          </div>
          <?php endif; ?>
          <div class="structure__<?=$typeBlock?>--content">
            <div class="structure__<?=$typeBlock?>--title title__section"><?=the_title()?></div>
            <div class="structure__<?=$typeBlock?>--text"><?=the_excerpt()?></div>
          </div>
        </div>
        <?php 
	        		$cout++;
	        	endwhile;
	        endif; 
	        wp_reset_query();
				?>
      </div>
    </section>

    <section class="gallery">
      <span class="gallery__title title__section line-detail">Galeria</span>
      <div class="container">
	      <select class="select-gallery" id="select-gallery">
		      <?php 
		        $args=array(
						  'orderby' => 'id',
						  'order' => 'ASC',
						  'taxonomy' => 'tipo_galeria'
					  );
						$categorias = get_categories($args);
	
						foreach($categorias as $i => $categoria):						
	        ?>
	        	<option value="tab-<?=$i?>" <?=($i == 0)?'selected':''?>><?=$categoria->name?></option>
	        <?php endforeach; ?>
        </select>
        <ul class="gallery__tab-list">
	        <?php 
		        $args=array(
						  'orderby' => 'id',
						  'order' => 'ASC',
						  'taxonomy' => 'tipo_galeria'
					  );
						$categorias = get_categories($args);

						foreach($categorias as $i => $categoria):						
	        ?>
	          <li class="gallery__tab">
	            <a href="javascript:void(0)" class="gallery__tab--link <?=($i == 0)?'active':''?>" data-tab="tab-<?=$i?>"><?=$categoria->name?></a>
	          </li>
          <?php endforeach; ?>
        </ul>

				<?php foreach($categorias as $i => $categoria): ?>
        	<div id="tab-<?=$i?>" class="gallery__content <?=($i == 0)?'active':''?>">
	          <ul class="gallery__content--list">
		          <?php  
								$args = array(
									'post_type' => 'galeria',
									'order'			=> 'ASC',
									'tax_query' => array(
						        array(
					            'taxonomy' => 'tipo_galeria',
					            'field'    => 'slug',
					            'terms'    => $categoria->name,
						        ),
							    ),
								);
								
								$loop = new WP_Query( $args );
								if( $loop->have_posts() ):
									while( $loop->have_posts() ):
										$loop->the_post();		
							?>
		            <li class="gallery__content--item">
		              <a class="image-link" href="<?=get_the_post_thumbnail_url(get_the_ID())?>">
		                <img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="<?=the_title()?>"/>
		              </a>
		            </li>
	            <?php 
				        	endwhile;
				        endif; 
				        wp_reset_query();
							?>
	          </ul>
	        </div>
        <?php endforeach; ?>
      </div>
    </section>

    <section id="event" class="event">
      <div class="container indentation__mid-page flex-row justify-between">
        <div class="event__content">
          <div class="event__content--title title__section white-text"><?=get_field('titulo_form')?></div>
          <p class="white-text event__content--text"><?=get_field('texto_form')?></p>
        </div>

        <div class="event__form">
          <?=do_shortcode('[contact-form-7 id="165" title="Contato"]')?>
        </div>
      </div>
    </section>

		<?php  
			$args = array(
				'post_type' 	=> 'parceiros',
				'order'			=> 'ASC',
				'orderby'		=>	'title'
			);
			
			$loop = new WP_Query( $args );
			if( $loop->have_posts() ):
		?>
    <section class="partners">
      <span class="partners__title title__section line-detail">Parceiros</span>
      <div class="container">
        <ul class="partners__list">
	        <?php  
				while( $loop->have_posts() ):
					$loop->the_post();		
					
					$imageParceiro = get_the_post_thumbnail_url(get_the_ID());
			?>
          <li class="partners__item">
            <div class="partners__item--image parceiros">
              <img src="<?=$imageParceiro?>" alt="<?=($imageParceiro)?the_title():''?>"/>
            </div>
            <div class="partners__item--text"><p><?php the_title() ?></p></div>
          </li>
          <?php endwhile; ?>
        </ul>
      </div>
    </section>
    <?php 
	    endif; 
	    wp_reset_query();
    ?>
  </main>

<?php get_footer(); ?>