<?php
/**
 * Template name: Ingressos
 *
 * @package TeatroPrevent
 */	
	get_header();
	
  $path = get_template_directory_uri();

?>

	<main class="ingressos grey">
    <section class="ingressos__content">
      <div class="container">

        <a class="button_top" href="#"></a>

        <div class="ingressos__menu">
          <p class="ingressos__menu--item"><a href="#resgatar">Como posso resgatar meu ingresso?</a></p>
          <p class="ingressos__menu--item"><a href="#onde-comprar">Onde comprar</a></p>
          <p class="ingressos__menu--item"><a href="#comprar-online">Como comprar online</a></p>
          <p class="ingressos__menu--item"><a href="#politica">Política de meia entrada</a></p>
          <p class="ingressos__menu--item"><a href="#duvidas">Dúvidas</a></p>
        </div>

        <div class="ingressos__wrapper">

        <?php if( have_rows('bloco') ): ?>
          <?php while ( have_rows('bloco') ) : the_row(); ?>
            <div class="ingressos__wrapper--bloco" id="<?php the_sub_field('id_menu'); ?>">

              <h2><?php the_sub_field('titulo'); ?></h2>

              <div class="ingressos__wrapper--bloco-itens">
                <h4 class="bloco1">PARA BENEFICIÁRIOS:</h4>
                
                  <ul>
                    <?php if( have_rows('itens') ): ?>
                      <?php while ( have_rows('itens') ) : the_row(); ?>
                        <li>
                          <div class="cabecalho">
                            <span><?php the_sub_field('numero'); ?></span>
                            <?php the_sub_field('texto'); ?>
                          </div>
                          <?php if( get_sub_field('subtexto') ) : ?>
                            <span class="subtitulo"><?php the_sub_field('subtexto'); ?></span>
                          <?php endif; ?>
                          <?php $imagem = get_sub_field('imagem'); ?>
                          <img src="<?php echo $imagem['url'] ?>" alt="<?php $imagem['alt'] ?>">

                          <?php if( get_sub_field('texto_interno') ) : ?>
                            <p>
                              <?php the_sub_field('texto_interno') ?>
                            </p>  
                          <?php endif; ?>                        

                        </li>
                      <?php endwhile; ?>
                    <?php endif; ?>
                                                            
                    <p class="bloco1">
                      <strong class="importante">IMPORTANTE</strong>
                      <p class="bloco1" style="margin-bottom: 90px;">Duvidas para finalizar o processo? Veja o item Como comprar online.</p>
                    </p>

                  </ul>
                  
                <div class="bloco1">
                  <h4>PARA COLABORADORES:</h4>
                  <ul>
                    <li><span>1.</span>Confira ingressos e espetáculos disponíveis acessando o <a href="#">Portal Web</a>.</li>
                  </ul>
                </div>
                
              </div>

              <hr>

            </div>
          <?php endwhile; ?>
        <?php endif; ?>

        <div class="ingressos__wrapper--bloco" id="duvidas">

          <div class="ingressos__wrapper--bloco-itens">
            <strong>DÚVIDAS?</strong>
            <p>Caso ainda precise de ajuda com a aquisição do seu ingresso, entre em contato com nossa equipe através do e-mail</p>
            <p><a href="mailto:atendimento@teatropreventsenior.com.br">atendimento@teatropreventsenior.com.br</a> com sua pergunta.</p>
          </div>
          
        </div>

        </div>

      </div>
    </section>
  </main>

<?php get_footer(); ?>

<script src="<?=$path?>/assets/js/ingressos.js"></script>