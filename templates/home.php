<?php
/**
 * Template name: Home
 *
 * @package TeatroPrevent
 */	
 
function cortaTexto($texto, $limit){
	if (strlen($texto) > $limit)
		return substr($texto, 0, $limit)."...";
	else
		return $texto;
}

function semHtml($string) {
	$string = str_replace("<br>", " ", $string);
	$string = str_replace("<BR>", " ", $string);
	$string = str_replace("<br />", " ", $string);
	$string = str_replace("<BR />", " ", $string);
	$string = str_replace("<em>", " ", $string);
	$string = str_replace("<EM>", " ", $string);
	$string = str_replace("</em>", " ", $string);
	$string = str_replace("<EM />", " ", $string);
	$string = str_replace("<i>", " ", $string);
	$string = str_replace("</i>", " ", $string);
	$string = str_replace("\n", " ", $string);
	$string = str_replace("\r", " ", $string);
	return strip_tags($string);
}
	
get_header();

$path = get_template_directory_uri();

?>
<main class="home">
  <section class="banner">
    <div class="swiper-container banner__container">
      <div class="swiper-wrapper">
	      <?php  
					$args = array(
						'post_type' => 'banner',
						'order'			=> 'ASC'
					);
					
					$loop = new WP_Query( $args );
					if( $loop->have_posts() ):
						while( $loop->have_posts() ):
							$loop->the_post();
				?>
	        <div class="swiper-slide">
            <?php
              $thumbnail = get_the_post_thumbnail_url(get_the_ID());

              $images = get_field('images');
              $desktopImage = $images['desktop_image'];
              $mobileImage = $images['mobile_image'];
            ?>

	          <?php if ($thumbnail): ?>
              <img class="banner__image" src="<?= $thumbnail ?>" alt="Teatro Prevent Senior"/>
            <?php else: ?>
              <?php if ($mobileImage): ?>
                <img class="banner__image desktop" src="<?= $desktopImage['url'] ?>" alt="<?= $desktopImage['alt'] ?>">
                <img class="banner__image mobile" src="<?= $mobileImage['url'] ?>" alt="<?= $mobileImage['alt'] ?>">
              <?php else: ?>
                <img class="banner__image" src="<?= $desktopImage['url'] ?>" alt="<?= $desktopImage['alt'] ?>">
              <?php endif; ?>
            <?php endif; ?>

	          <div class="banner__float-content">
              <?php if (get_field('banner_titulo')): ?>
	              <div class="banner__title"><?php the_title() ?></div>
              <?php endif; ?>

	            <div class="banner__sub-title"><?=get_field('sub_titulo', get_the_ID())?></div>
              
              <?php if (get_field('texto_botao', get_the_ID())): ?>
	              <a href="<?=get_field('link_botao', get_the_ID())?>" class="button"><?=get_field('texto_botao', get_the_ID())?></a>
              <?php endif ?>
	          </div>
	        </div>
        <?php 
	        	endwhile;
	        endif; 
	        wp_reset_query();
				?>
      </div>
      <div class="banner__navigation container">
        <div class="banner__navigation--button prev arrow-white__left"></div>
        <div class="banner__navigation--button next arrow-white__right"></div>
      </div>
      <div class="swiper-pagination"></div>
    </div>
  </section>

  <section class="about grey base">
    <div class="container flex-row">
      <div class="about__content indentation__page half-on-medium">
        <span class="title__section black-text"><?=get_field('titulo_sobre')?></span>
        <p class="indentation__text grey-text text-darken"><?=get_field('texto_sobre')?></p>
      </div>
      <?php 
	      $imagem_sobre = get_field('imagem_sobre'); 
				if($imagem_sobre):
	     ?>
      <div class="about__image half-on-medium">
        <div class="about__image--container image-frame">
          <img src="<?=$imagem_sobre['url']?>" alt="<?=$imagem_sobre['alt']?>">
        </div>
      </div>
      <?php endif; ?>
    </div>
  </section>

  <section class="structure">
    <div class="container">
      <div class="structure__title title__section">Estrutura</div>
      <div class="swiper-container structure__slider">
        <div class="structure__navigation">
          <div class="structure__navigation--button prev arrow__left"></div>
          <div class="structure__navigation--button next arrow__right"></div>
        </div>
        <div class="swiper-wrapper">
	        <?php  
		        $estruturas = get_field('estruturas');
		        foreach($estruturas as $estrutura):
							$metas = get_post_meta($estrutura->ID);
							$gallery = $metas['galeria'];			
							
							$postContent = semHtml($estrutura->post_content);
							$conteudo = $postContent;			
					?>
          <div class="swiper-slide">
            <div class="structure__gallery">
	            <?php if($gallery): ?>
              <div class="swiper-container swiper-gallery">
                <div class="swiper-wrapper">
	                <?php foreach($gallery as $image): ?>
                  <div class="swiper-slide">
                    <div class="swiper-gallery__image image-frame">
                      <img src="<?=pods_image_url($image, 'full')?>" alt="Teatro Prevent Senior"/>
                    </div>
                  </div>
                  <?php endforeach; ?>
                </div>
                <div class="swiper-gallery__pagination swiper-pagination"></div>
              </div>
              <?php endif; ?>
            </div>
            <div class="structure__content">
              <div class="structure__content--title"><?=$estrutura->post_title?></div>
              <p class="structure__content--text"><?=$conteudo?></p>
              <a href="<?=bloginfo('url')?>/faca-seu-evento" class="button">Saiba mais</a>
            </div>
          </div>
          <?php 
	          endforeach;
					?>
        </div>
      </div>
    </div>
  </section>

  <section class="programming">
    <div class="programming__content">
      <span class="programming__content--title title__section white-text"><?=get_field('titulo_prog')?></span>
      <p class="white-text"><?=get_field('descricao_prog')?></p>
      <a href="<?=bloginfo('url')?>/programacao" class="button border">Ver todos os eventos</a>
    </div>
    <div class="programming__slider">
      <div class="swiper-container swiper-programming">
        <div class="swiper-wrapper">
	        <?php  
						$args = array(
							'post_type' => 'evento',
              'order'			=> 'ASC',
              'orderby'   => 'meta_value_num',
              'meta_key'  => 'data',
              'meta_query' => array(
                array(
                  'key' => 'data',
                  'value' => date("Y-m-d"),
                  'compare' => '>=',
                  'type' => 'DATE'
                )
              )
						);
						
						$loop = new WP_Query( $args );
						if( $loop->have_posts() ):
							while( $loop->have_posts() ):
								$loop->the_post();
					?>
	          <a href="<?=get_permalink(get_the_ID())?>" class="swiper-slide programming__item">
	            <div class="programming__item--image">
	              <img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="Programação Teatro Prevent Senior">
	            </div>
	            <div class="programming__item--title"><?=the_title()?></div>
	            <div class="programming__item--date"><?=get_field('sub-titulo', get_the_ID())?><?php get_field('data'); ?></div>
	          </a>
	        <?php 
		        	endwhile;
		        endif; 
		        wp_reset_query();
          ?>
        </div>
        <div class="programming__navigation container">
          <div class="programming__navigation--button prev arrow-white__left"></div>
          <div class="programming__navigation--button next arrow-white__right"></div>
        </div>
      </div>
    </div>
  </section>

	<?php  
		$args = array(
			'post_type' => 'post',
      'order'			=> 'DESC',
      'posts_per_page' => 3
		);
		
		$loop = new WP_Query( $args );
		if( $loop->have_posts() ):
	?>
  <section class="blog grey base">
    <span class="blog__title title__section line-detail"><?=get_field('titulo_blog')?></span>
    <div class="container">
      <ul class="blog__list post">
	      <?php  
		      while( $loop->have_posts() ):
						$loop->the_post();
	      ?>
        <li class="post__item">
          <a href="<?=get_permalink(get_the_ID())?>" class="post__item--image">
            <img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="<?=the_title()?>"/>
          </a>
          <a href="<?=get_permalink(get_the_ID())?>" class="post__item--content">
            <div class="post__item--title"><?=the_title()?></div>
            <div class="post__item--text"><?=the_excerpt()?></div>
            <div class="post__item--date"><?=get_the_date()?></div>
            <span class="post__item--button button">Leia mais</span>
          </a>
        </li>
        <?php endwhile; ?>
      </ul>
      <a href="<?=bloginfo('url')?>/blog" class="button border black-text margin-auto">Acessar o blog</a>
    </div>
  </section>
  <?php endif; wp_reset_query(); ?>
</main>

<?php get_footer(); ?>