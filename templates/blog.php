<?php
/**
 * Template name: Blog
 *
 * @package TeatroPrevent
 */	
	global $wp_query;

	$blog = $wp_query->queried_object;
	$path = get_template_directory_uri();

	get_header();
?>
 <main class="blog">
    <div class="container">
      <div class="blog__content">
        <h2 class="blog__title-page title__section"><?= get_field('cabecalho_blog', $blog->ID) ?></h2>
        <?php  
					$args = array(
						'post_type' => 'post',
						'order'			=> 'DESC',
						'posts_per_page' => 1,
						'tax_query' => array(
				        array(
			            'taxonomy' => 'category',
			            'field'    => 'slug',
			            'terms'    => 'destaque',
				        ),
					    ),
					);
					
					$loop = new WP_Query( $args );
					if( $loop->have_posts() ):
						while( $loop->have_posts() ):
							$loop->the_post();
							$category = end(get_the_category(get_the_ID()));
				?>
	        <div class="featured">
	          <a href="<?=get_permalink(get_the_ID())?>" class="featured__image">
	            <img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="Teatro Prevent Senior"/>
	          </a>
	          <div class="featured__content red white-text">
	            <div class="featured__infos">
	              <span class="featured__infos--date"><?=the_date()?></span>
	              <span class="featured__infos--label"><?=$category->name?></span>
	            </div>
	            <a href="<?=get_permalink(get_the_ID())?>" class="featured__title white-text"><?=the_title()?></a>
	            <div class="featured__exerpt"><?=the_excerpt()?></div>
	          </div>
	        </div>
        <?php 
	        			endwhile;
	        		endif; 
	        		wp_reset_postdata(); 
	      ?>
      </div>

		<?php if( have_posts() ): ?>
      	
			<ul class="post">
			
				<?php while( have_posts() ): the_post(); ?> 
					
					<li class="post__item">
						<a href="<?=get_permalink(get_the_ID())?>" class="post__item--image">
							<img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="<?=the_title()?>"/>
						</a>
						<a href="<?=get_permalink(get_the_ID())?>" class="post__item--content">
							<div class="post__item--title"><?=the_title()?></div>
							<p class="post__item--text"><?=the_excerpt()?></p>
							<div class="post__item--date"><?=the_date()?></div>
							<span class="post__item--button button">Leia mais</span>
						</a>
					</li>

				<?php endwhile; wp_reset_query(); ?>
			
			</ul>

			<div class="pagination-nav">
				<div class="container">
					<div class="nav-holder">
						<?php echo paginate_links(array(
							'prev_text' => __('Anterior'),
							'next_text' => __('Próxima'),
						)); ?>
					</div>
				</div>
			</div>

		<?php endif; ?>
      
    </div>
  </main>
<?php get_footer(); ?>