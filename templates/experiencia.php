<?php
/**
 * Template name: Experiência
 *
 * @package TeatroPrevent
 */	
	get_header();
	
	$path = get_template_directory_uri();

?>
	<main class="experiencia">
    <section class="banner">
      <img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="<?=the_title()?>"/>
    </section>

    <section class="content">
      <div class="container indentation__mid-page">
        <div class="content__item initial">
          <h1 class="content__item--title title__section"><?=get_field('titulo_xp')?></h1>
          <div class="content__item--text indentation__mid-page">
            <p><?=get_field('sub-titulo_xp')?></p>
          </div>
        </div>
        <?php 
	        while ( have_rows('andar_1') ) : the_row(); 
	        	$image1 = get_sub_field('imagem');
	      ?>
        <div class="content__item">
          <h1 class="content__item--title title__section"><?=get_sub_field('titulo')?></h1>
          <div class="content__item--text indentation__mid-page">
            <p><?=get_sub_field('texto')?></p>
          </div>
          <div class="content__item--image">
            <img src="<?=$image1['url']?>" alt="<?=$image1['alt']?>"/>
          </div>
          <a href="" class="content__item--button button margin-auto">
            <img src="<?=$path?>/assets/images/360xp.svg" alt="Globo mundi"/>
            <span>VEJA EM 360 GRAUS</span>
          </a>
        </div>
        <?php 
	        endwhile; 
					
					while ( have_rows('andar_2') ) : the_row(); 
						$image2 = get_sub_field('imagem');
	      ?>
        <div class="content__item">
          <h1 class="content__item--title title__section"><?=get_sub_field('titulo')?></h1>
          <div class="content__item--text indentation__mid-page">
            <p><?=get_sub_field('texto')?></p>
          </div>
          <div class="content__item--image">
            <img src="<?=$image2['url']?>" alt="<?=$image2['alt']?>"/>
          </div>
          <a href="" class="content__item--button button margin-auto">
            <img src="<?=$path?>/assets/images/360xp.svg" alt="Globo mundi"/>
            <span>VEJA EM 360 GRAUS</span>
          </a>
        </div>
        <?php 
	        endwhile; 
					
					while ( have_rows('andar_3') ) : the_row(); 
						$image3 = get_sub_field('imagem');
	      ?>
        <div class="content__item">
          <h1 class="content__item--title title__section"><?=get_sub_field('titulo')?></h1>
          <div class="content__item--text indentation__mid-page">
            <p><?=get_sub_field('texto')?></p>
          </div>
          <div class="content__item--image">
            <img src="<?=$image3['url']?>" alt="<?=$image3['alt']?>"/>
          </div>
          <a href="" class="content__item--button button margin-auto">
            <img src="<?=$path?>/assets/images/360xp.svg" alt="Globo Mundi"/>
            <span>VEJA EM 360 GRAUS</span>
          </a>
        </div>
        <?php 
	        endwhile; 
					
					while ( have_rows('andar_4') ) : the_row(); 
						$image4 = get_sub_field('imagem');
	      ?>
        <div class="content__item">
          <h1 class="content__item--title title__section"><?=get_sub_field('titulo')?></h1>
          <div class="content__item--text indentation__mid-page">
            <p><?=get_sub_field('texto')?></p>
          </div>
          <div class="content__item--image">
            <img src="<?=$image4['url']?>" alt="<?=$image4['alt']?>"/>
          </div>
          <a href="" class="content__item--button button margin-auto">
            <img src="<?=$path?>/assets/images/360xp.svg" alt="Globo Mundi"/>
            <span>VEJA EM 360 GRAUS</span>
          </a>
        </div>
        <?php endwhile; ?>
      </div>
    </section>
  </main>
<?php get_footer(); ?>