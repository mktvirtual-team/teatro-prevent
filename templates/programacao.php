<?php
/**
 * Template name: Programação
 *
 * @package TeatroPrevent
 */	
	get_header();
	
	$path = get_template_directory_uri();
	
	$tipo = $_GET['tipo'];
	$quando = $_GET['quando'];
	
	$selectTipo = get_categories(
		array(
		  'orderby' => 'id',
		  'order' => 'ASC',
		  'taxonomy' => 'tipo'
	  )
	);
	$selectQuando = get_categories(
		array(
		  'orderby' => 'name',
		  'order' => 'ASC',
		  'taxonomy' => 'data'
	  )
	);
	
?>

	<main class="programacao grey">
		<?php if($quando || $tipo != 'todos' && $tipo != ''): ?>
		<section></section>
		<?php else: ?>
    <section class="slider">
      <div class="swiper-container swiper-programming container">
        <div class="swiper-wrapper">
	        <?php  
						$args = array(
							'post_type' => 'evento',
							'order'			=> 'ASC',
							'posts_per_page' => 8,
							'orderby'   => 'meta_value_num',
							'meta_key'  => 'data',
							'meta_query' => array(
								array(
									'key' => 'data',
									'value' => date("Y-m-d"),
									'compare' => '>=',
									'type' => 'DATE'
								)
							)
						);
						
						$loop = new WP_Query( $args );
						if( $loop->have_posts() ):
							while( $loop->have_posts() ):
								$loop->the_post();
					?>
	          <div class="swiper-slide slider__item">
	            <div class="slider__item--image">
	              <img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="Teatro Prevent Senior">
	            </div>
	            <div class="slider__item--title"><?=the_title()?></div>
	            <div class="slider__item--date"><?=get_field('sub-titulo', get_the_ID())?></div>
				<a href="<?=get_permalink(get_the_ID())?>" class="button" style="margin-right: unset;">Saiba Mais</a>
	          </div>
          <?php 
		        	endwhile;
		        endif; 
		        wp_reset_query();
					?>
        </div>
        <div class="slider__pagination swiper-pagination"></div>
      </div>
    </section>
    <?php endif; ?>

    <section class="calendar">
      <div class="container">
        <div class="calendar__header indentation__page">
          <h2 class="title__section">Calendário<br/> de Programação</h2>
		  <?php /*
          <form class="calendar__header--filters">
            <label>FIltrar por: </label>
            <select id="tipoSelect" class="select" name="tipo">
              <option disabled selected>Tipo de Evento</option>
              <?php foreach($selectTipo as $option): ?>
              	<option <?=($option->slug == $tipo) ? 'selected' : ''?> value="<?=$option->slug?>"><?=$option->name?></option>
              <?php 
	              endforeach; 
	              
	              if($tipo || $quando): ?>
									<option value="todos">Ver todos</option>
							<?php endif; ?>
            </select>
            <select id="quandoSelect" class="select" name="quando">
              <option disabled selected>Quando</option>
	              <?php  
									$args = array(
										'post_type' => 'evento',
										'order'			=> 'ASC',
										'orderby'   => 'meta_value_num',
										'meta_key'  => 'data',
										'meta_query' => array(
											array(
												'key' => 'data',
												'value' => date("Y-m-d"),
												'compare' => '>=',
												'type' => 'DATE'
											)
										)
									);
									
									$loop = new WP_Query( $args );
									if( $loop->have_posts() ):
										$auxH = '';
										while( $loop->have_posts() ):
											$loop->the_post();
											$category = end(get_the_terms(get_the_ID(), 'tipo'));
											
											$auxH = ($auxH != get_field('data')) ? get_field('data') : null;
											
											if($auxH != null):
											
												$time = strtotime($auxH);
												
												setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
												date_default_timezone_set('America/Sao_Paulo');
												
												$newDate = strftime('%d de %B', strtotime($auxH));
								?>
	              	<option <?=($auxH == $quando) ? 'selected' : ''?> value="<?=$auxH?>"><?=utf8_encode($newDate)?></option>
              	<?php 
	              			endif;
					        	endwhile;
					        endif; 
					        wp_reset_query();
								?>
            </select>
		  </form>
		  */?>
        </div>
        <?php if($quando || $tipo != 'todos' && $tipo != ''): ?>
        	<div class="calendar__content">
	        	<div class="calendar__content--date-wrapper">
		        	<ul class="calendar__list">
		            <?php  
			            $taxQuery = [];
			            $metaQuery = [];
							    
							    if ($tipo) {
								    $taxQuery = array(
										array(
											'taxonomy' => 'tipo',
											'field'    => 'slug',
											'terms'    => $tipo
										)
									);
								}
			            
									$args = array(
										'post_type'  => 'evento',
										'order'			 => 'ASC',
										'tax_query'  => $taxQuery,
										'meta_query' => array(
											array(
												'key' => 'data',
												'value' => date("Y-m-d"),
												'compare' => '>=',
												'type' => 'DATE'
											)
										)
									);
									
									$loop = new WP_Query( $args );
									if( $loop->have_posts() ):
										while( $loop->have_posts() ):
											$loop->the_post();
								?>
		              <li class="calendar__item indentation__full-page">
		                <div class="calendar__item--image">
		                  <img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="Calendário Teatro Prevent Senior"/>
		                </div>
		                <div class="calendar__item--content">
		                  <div class="calendar__item--label"><?= get_event_category_name() ?></div>
		                  <h5 class="calendar__item--title"><?=the_title()?></h5>
		                  <div class="calendar__item--text"><?=the_excerpt()?></div>
		                  <div class="calendar__item--date"><?=get_field('sub-titulo', get_the_ID())?></div>
		                  <a href="<?=get_permalink(get_the_ID())?>" class="button">Saiba Mais</a>
		                </div>
		              </li>
	              <?php 
					        	endwhile;
					        	
					        	else:
					       ?>
					       <p style="text-align: center">Nenhum resultado para esse filtro.</p>
					       <?php
					        endif; 
					        wp_reset_query();
								?>
	            </ul>
	        	</div>
        	</div>
        <?php else: ?>
        <div class="calendar__content">
          <div class="calendar__content--date-wrapper">
            <ul class="calendar__list">
	            <?php  
								$args = array(
									'post_type' => 'evento',
									'order'			=> 'ASC',
									'orderby'   => 'meta_value_num',
									'meta_key'  => 'data',
									'meta_query' => array(
										array(
											'key' => 'data',
											'value' => date("Y-m-d"),
											'compare' => '>=',
											'type' => 'DATE'
										)
									)
								);
								
								$loop = new WP_Query( $args );
								if( $loop->have_posts() ):
									$auxH = '';
									while( $loop->have_posts() ):
										$loop->the_post();
										
										$auxH = ($auxH != get_field('data')) ? get_field('data') : null;
										
										if ($auxH != null) {
											$time = strtotime($auxH);
											
											setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
											date_default_timezone_set('America/Sao_Paulo');
											
											$newDate = strftime('%d de %B', strtotime($auxH)); 
											$newDate = utf8_encode($newDate); 
										}
							?>
								<?php if($auxH != null): ?>
									<div class="calendar__content--date-title indentation__full-page"><?=$newDate?></div>
								<?php endif; ?>
	              <li class="calendar__item indentation__full-page">
	                <div class="calendar__item--image">
	                  <img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="Calendário Teatro Prevent Senior"/>
	                </div>
	                <div class="calendar__item--content">
	                  <div class="calendar__item--label"><?= get_event_category_name() ?></div>
	                  <h5 class="calendar__item--title"><?=the_title()?></h5>
	                  <div class="calendar__item--text"><?=the_excerpt()?></div>
	                  <div class="calendar__item--date"><?=get_field('sub-titulo', get_the_ID())?></div>
	                  <a href="<?=get_permalink(get_the_ID())?>" class="button">Saiba Mais</a>
	                </div>
	              </li>
              <?php 
				        	endwhile;
				        endif; 
				        wp_reset_query();
							?>
            </ul>
          </div>
        </div>
		<?php endif; ?>
		<div class="eventos-realizados indentation__page">
		  <h2>Espetáculos<br/> Realizados</h2>
		  <a href="<?=bloginfo('url')?>/programacao-passada" class="button">Clique aqui para acessar</a>
		</div>
      </div>
    </section>
  </main>

<?php get_footer(); ?>