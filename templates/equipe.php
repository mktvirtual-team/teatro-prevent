<?php
/**
 * Template name: Equipe
 *
 * @package TeatroPrevent
 */	
	get_header();
	
	$path = get_template_directory_uri();
	
?>

	<main class="equipe grey">
    <section class="equipe__content">
      <!-- <h2 class="equipe__title-page title__section">Conheça Nossa Equipe</h2> -->
      <div class="container">
        <div class="equipe__wrapper">
	        <?php  
						$args = array(
							'post_type' => 'equipe',
							'order'			=> 'ASC',
						);
						
						$loop = new WP_Query( $args );
						if( $loop->have_posts() ):
							while( $loop->have_posts() ):
								$loop->the_post();
								
								$image = get_the_post_thumbnail_url(get_the_ID());
					?>
	          <div class="equipe__item">
	            <div class="equipe__item--image">
	              <img src="<?=($image) ? $image : $path.'/assets/images/person_default.png'?>" alt="Equipe Teatro Prevent Senior"/>
	            </div>
	            <div class="equipe__item--texts">
	              <div class="equipe__item--wrapper">
	                <span class="equipe__item--name"><?=the_title()?></span>
	                <span class="equipe__item--office"><?=get_field('cargo')?></span>
	              </div>
	              <p><?=get_field('descricao')?></p>
	              <div class="equipe__item--wrapper">
	                <span class="equipe__item--tel"><?=get_field('telefone')?></span>
	                <span class="equipe__item--email"><?=get_field('email')?></span>
	              </div>
	            </div>
	          </div>
          <?php 
		        	endwhile;
		        endif; 
		        wp_reset_query();
					?>
        </div>
      </div>
    </section>
  </main>

<?php get_footer(); ?>