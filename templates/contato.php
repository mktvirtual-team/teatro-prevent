<?php
/**
 * Template name: Contato
 *
 * @package TeatroPrevent
 */	
	get_header();
	
	$path = get_template_directory_uri();
?>

	<main class="contato grey">
    <section class="contato__init">
      <div class="container indentation__mid-page">
        <h2 class="contato__title-page title__section"><?=the_title()?></h2>
        <div class="contato__init--wrapper">
          <!-- <span class="contato__sub-title">Onde estamos?</span> -->
          <div class="contato__list">
            <!-- <div class="contato__item">
              <span class="contato__item--title">ENDEREÇO</span>
              <p class="contato__item--text"><?//=get_field('endereco')?></p>
            </div> -->
            <div class="contato__init--item">
              <img class="icon-tel" src="<?php echo get_template_directory_uri();?>/assets/images/call-answer.png" alt="Telefone para contato">
              <!-- <span class="contato__item--title justify-center">TELEFONE</span> -->
              <p class="contato__item--text center"><b>COMERCIAL:</b> <?=get_field('telefone_comercial')?></p>
              <p class="contato__item--text center"><b>ATENDIMENTO:</b> <?=get_field('telefone_atendimento')?></p>
            </div>
            <div class="contato__init--item">
              <span class="contato__item--title justify-center">HORÁRIO DE ATENDIMENTO:</span>
              <p class="contato__item--text center"><?=get_field('horario_de_atendimento')?></p>
            </div>
            <div class="contato__init--item">
              <!-- <span class="contato__item--title justify-center">ENDEREÇO</span> -->
              <p class="contato__item--text center"><?=get_field('endereco')?></p>
            </div>
            <div class="contato__init--item bilheteria">
              <p class="contato__item--text center bold horario title"><?=get_field('bilheteria_titulo')?></p>
              <p class="contato__item--text center bigger"><?=get_field('bilheteria_horario')?></p>
              <p class="contato__item--text center"><?=get_field('bilheteria_excecao')?></p>
              <p class="contato__item--text center link title"><?=get_field('link_title')?> <a href="<?=get_field('link')?>"><?=get_field('link_text')?></a></p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="contato__map">
     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.2100212174855!2d-46.69667538447001!3d-23.560899384683093!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce57a3648592e9%3A0x9f9145570e7db18f!2sRua%20Corop%C3%A9%2C%2088%20-%20Pinheiros%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2005426-010!5e0!3m2!1spt-BR!2sbr!4v1568818569224!5m2!1spt-BR!2sbr" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </section>

    <section class="contato__attendance">
      <div class="container indentation__mid-page flex-column align-center">
        <span class="contato__sub-title">ATENDIMENTO AO PÚBLICO</span>
        <div class="contato__col-list">
	        <div class="contato__attendance--item margin-bottom">
		        <p class="contato__item--text"><?=get_field('email')?></p>
	        </div>
        </div>
      </div>
    </section>
  </main>

<?php get_footer(); ?>