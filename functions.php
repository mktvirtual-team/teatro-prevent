<?php
	
function get_titulo() {
	if( is_home() ) {
		bloginfo('name');
	} else {
		bloginfo('name');
		echo ' | ';
		the_title();
	}
}

add_action( 'init', 'registrar_menu_navegacao');

function registrar_menu_navegacao() {
	register_nav_menu('header-menu', 'Menu Header');
}

add_theme_support( 'post-thumbnails' );

function add_svg_to_upload_mimes( $upload_mimes ) {
	$upload_mimes['svg'] = 'image/svg+xml';
	$upload_mimes['svgz'] = 'image/svg+xml';
	
	return $upload_mimes;
}

function reset_editor() {
	global $_wp_post_type_features;
	
	$post_type="page";
	$features = array("editor", "comments", "post_name", "author");
	
	foreach($features as $feature){
		if ( isset($_wp_post_type_features[$post_type][$feature]) ) {
			unset($_wp_post_type_features[$post_type][$feature]);
		}
	}
}

add_action("init","reset_editor");

/* Action para o registro da Custom Taxonomy Tipo de Produto */
add_action( 'init', 'season_event' );

/* Método para o registro da Custom Taxonomy Tipo de Produto */ 
function season_event(){
    $custom_tax_nome = 'season_event';
    $custom_post_type_nome = 'evento';
    $args = array(
		'label' => __('Temporada'),
		'meta_box_cb' => 'post_categories_meta_box',
        'rewrite' => array('slug' => 'Temporada')
    );
    register_taxonomy( $custom_tax_nome, $custom_post_type_nome, $args );
}

function get_events_by_season(WP_Term $season){
	// if ( false === ( $events = get_transient( 'transient_events' ) ) ) {

		$events = new WP_Query(array(
			'post_type' => 'evento',
			'posts_per_page' => -1,
			'order'			=> 'DESC',
			'orderby'   => 'meta_value_num',
			'meta_key'  => 'data',
			'meta_query' => array(
				array(
					'key' => 'data',
					'value' => date("Y-m-d"),
					'compare' => '<',
					'type' => 'DATE'
				)
			),
			'tax_query' => array(
				array(
					'taxonomy' => 'season_event',
					'field'    => 'name',
					'terms'    => $season->name,
				),
			),
		));

		return $events;

		// set_transient( 'transient_events', $events, 12 * HOUR_IN_SECONDS );
	// }
}

// Create a simple function to delete our transient
// function edit_term_delete_transient() {
// 	delete_transient( 'transient_events' );
// }

// Add the function to the edit_term hook so it runs when categories/tags are edited
add_action( 'edit_term', 'edit_term_delete_transient' );

function get_event_category_name(){
	if(!$categories = get_the_terms(get_the_ID(), 'tipo')){
		return 'Sem Categoria';
	}

	$category_names = array();

	foreach($categories as $category){
		$category_names[] = $category->name;
	}

	return implode(', ', $category_names);
}
