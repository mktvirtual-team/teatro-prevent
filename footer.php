<?php 
  global $post;

  $path = get_template_directory_uri();

  $post_slug = isset( $post->post_name ) ? $post->post_name : '404' ;

  if ( is_404() ) {
    $post_slug = '404';
  }
	
	$menu1 = wp_get_nav_menu_items('INGRESSOS');
	$menu2 = wp_get_nav_menu_items('SOBRE');
	$menu3 = wp_get_nav_menu_items('EVENTOS');
	
	$Infos = pods( 'informacoes_rodape', null );
	$link_instagram = $Infos->display('instagram');
	$link_facebook = $Infos->display('facebook');
	$telefone_1 = $Infos->display('telefone_1');
	$telefone_2 = $Infos->display('telefone_2');
	$telefone_3 = $Infos->display('telefone_3');
?>
		
		<footer class="footer black">
      <div class="footer__bottom">
        <div class="container flex-row">
          <div class="footer__bottom--block">

            <a href="" class="footer__bottom--logo">
              <img src="<?=$path?>/assets/images/logo-footer.png" alt="Teatro Prevent Senior"/>
            </a>

            <div class="footer__bottom--social">
              <div class="footer__bottom--social-title white-text">Siga-nos nas redes socias</div>

              <?php if($link_instagram): ?>
              <a target="_blank" href="<?=$link_instagram?>" class="footer__bottom--social-link">
                <img src="<?=$path?>/assets/images/icone-instagram.svg" alt="Instagram - Teatro Prevent Senior"/>
              </a>
              <?php endif; ?>

              <?php if($link_facebook): ?>
              <a target="_blank" href="<?=$link_facebook?>" class="footer__bottom--social-link">
                <img src="<?=$path?>/assets/images/icone-facebook.svg" alt="Facebook - Teatro Prevent Senior"/>
              </a>
              <?php endif; ?>

            </div>
          </div>
          
	        <?php if(count($menu1) > 0): ?>
          <div class="footer__bottom--block">
            <div class="footer__bottom--label">Ingressos</div>
            <ul class="footer__bottom--list">
              <?php foreach($menu1 as $item): ?>
                <li class="footer__bottom--item">
	                <a href="<?=$item->url?>" class="<?= ( $item->target !== "" ) ? 'pop-up' : '' ?>"><?=$item->title?></a>
	              </li>
              <?php endforeach; ?>
            </ul>
          </div>

          <?php 
	          endif;
	          
	          if(count($menu2) > 0): 
	        ?>
          <div class="footer__bottom--block">
            <div class="footer__bottom--label">Sobre Nós</div>
            <ul class="footer__bottom--list">
              <?php foreach($menu2 as $item): ?>
	              <li class="footer__bottom--item">
	                <a href="<?=$item->url?>"><?=$item->title?></a>
	              </li>
              <?php endforeach; ?>
            </ul>
          </div>

          <?php 
	          endif;
	          
	          if(count($menu3) > 0): 
	        ?>
          <div class="footer__bottom--block">
            <div class="footer__bottom--label">Eventos Corporativos</div>
            <ul class="footer__bottom--list">
              <?php foreach($menu3 as $item): ?>
	              <li class="footer__bottom--item">
	                <a href="<?=$item->url?>"><?=$item->title?></a>
	              </li>
              <?php endforeach; ?>
            </ul>
          </div>
          <?php endif; ?>
          <div class="footer__bottom--block">
            <div class="footer__bottom--label">Central de Informações</div>
            <ul class="footer__bottom--list">
              <li class="footer__bottom--item"><?=$telefone_1?></li>
              <li class="footer__bottom--item"><?=$telefone_2?></li>
              <?php if($telefone_3) : ?>
                <li class="footer__bottom--item"><?=$telefone_3?></li>
              <?php endif; ?>
            </ul>
          </div>
        </div>
      </div>
      <div class="container footer-signatures">
        <span>Copyright © <?php echo date("Y"); ?> <strong>Teatro Prevent Senior</strong> - Todos os direitos reservados</span>
      </div>
    </footer>
    
    <script src="<?=$path?>/assets/js/jquery.js"></script>
    <script src="<?=$path?>/assets/js/magnific.popup.min.js"></script>
    <script src="<?=$path?>/assets/js/swiper.min.js"></script>
    <script src="<?=$path?>/assets/js/init.js"></script>
    <link rel="stylesheet" href="<?=$path?>/assets/css/style.css">
  </body>
</html>