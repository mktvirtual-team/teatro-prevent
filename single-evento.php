<?php
	get_header();
	
	$path = get_template_directory_uri();
	$category = end(get_the_category(get_the_ID()));
	
	$urVideo = get_field('video');
	
	if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $urVideo, $match)) {
	   	$videoId = $match[1];
	}
	
	$banner = get_field('banner');
	$embed = 'https://www.youtube.com/embed/'.$videoId;
	
	$ingressos = get_field('ingressos');
	
	while( have_posts() ):
		the_post();
		$thisID = get_the_ID();
?>
	
	<main class="evento">
    <section class="banner">
      <?php if($urVideo): ?>
	      <a href="#modal-video" class="open-popup-link">
		      <img src="<?=$banner['url']?>" alt="<?=$banner['alt']?>"/>
		      <img class="float-image" src="<?=$path?>/assets/images/play.png" alt="Player - Teatro Prevent Senior" />
	      </a>
	      <div id="modal-video" class="modal mfp-hide">
		      <iframe src="<?=$embed?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	      </div>
      <?php else: ?>
	      <img src="<?=$banner['url']?>" alt="<?=$banner['alt']?>"/>
	    <?php endif; ?>
    </section>

    <section class="intro grey">
      <div class="container">
        <div class="intro__box grey">
			<div class="flex-title">
				<div class="intro__box--image">
					<img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" alt="Teatro Prevent Senior"/>
				</div>
				<h2 class="intro__box--title"><?=the_title()?></h2>
			</div>
          <div class="intro__box--text"><?=the_content()?></div>
        </div>
      </div>
    </section>
    <?php endwhile; ?>

    <section class="dates grey">
      <div class="container">
        <div class="dates__title">Datas</div>
        <ul class="dates__list">
	        <?php 
	        	foreach($ingressos as $ingresso): 
	        		$titleIngresso = get_field('sub-titulo', get_the_ID());
	        		$linkIngresso = get_field('link_ingresso', $ingresso->ID);
		      ?>
	          <li class="dates__item">
				<span class="dates__item--name"><?=$titleIngresso?></span>
				<?php if( get_field('tickets_colaboradores', $ingresso->ID) ): ?>
					<a href="<?php echo get_template_directory_uri() ?>/assets/images/pop-up.png" class="button pop-up">INGRESSOS</a>
				<?php elseif( $linkIngresso ): ?>
	            	<a target="_blank" href="<?=$linkIngresso?>" class="button">INGRESSOS</a>
				<?php endif; ?>
	          </li>
	        <?php endforeach; ?>
        </ul>
      </div>
    </section>
  </main>

<?php get_footer(); ?>