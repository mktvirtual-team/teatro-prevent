<?php 
	$path = get_template_directory_uri(); 
	
	global $post;
	$post_slug = isset( $post->post_name ) ? $post->post_name : '404' ;
	
	$menu = wp_get_nav_menu_items('Menu');	
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-N6R6ZFQ');</script>
  <!-- End Google Tag Manager -->

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="format-detection" content="telephone=no">
  <title>Teatro Prevent Senior</title>

  <link rel="shortcut icon" type="image/png" href="<?=$path?>/assets/images/favicon.png"/>

  <link rel="stylesheet" href="<?=$path?>/assets/css/init.css">
  
  <?php wp_head(); ?>
</head>
<body <?php body_class( ) ?>>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N6R6ZFQ"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  
  <header class="header">
    <div class="container flex-row">
      <a href="<?=bloginfo('url')?>" class="logo">
        <img src="<?=$path?>/assets/images/logo.png" alt="Teatro Prevent Senior"/>
      </a>
      <ul class="menu">
	      <?php foreach($menu as $item): ?>
	        <li class="menu__item">
	          <a class="menu__item--link" href="<?=$item->url?>"><?=$item->title?></a>
	        </li>
        <?php endforeach; ?>
      </ul>
    </div>
    <a href="javascript:void(0)" class="hamburguer"><span></span></a>
    <a href="<?=bloginfo('url')?>/faca-seu-evento/#event" class="event-button">
      <img src="<?=$path?>/assets/images/icon-calendar.svg" alt="Calendário - Teatro Prevent Senior">
      <span>Faça seu evento</span>
    </a>
  </header>